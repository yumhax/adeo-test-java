package adeo.leroymerlin.cdp;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Band {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(fetch=FetchType.EAGER,cascade = {CascadeType.ALL})
    private Set<Member> members;

    public Set<Member> getMembers() {
        return members;
    }

    public void setMembers(Set<Member> members) {
        this.members = members;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public Band(Band band){
        this.id = band.id;
        this.members = band.members;
        this.name = band.name;
    }

    public Band(){}

    public Band filterMembers(String filter){
        Set<Member> filteredMembers = this.members.stream()
                                    .map(member -> member.filterInternal(filter))
                                    .filter(member -> !member.name.isEmpty())
                                    .collect(Collectors.toSet());
        
        Band copyBand = new Band(this);
        copyBand.setMembers(filteredMembers);
        return copyBand;
    }

    public Band enrichBand(){
        Band copyBand = new Band(this);
        copyBand.setName(copyBand.name + " [" + Integer.toString(this.members.size()) + ']');
        return copyBand;
    }
}
