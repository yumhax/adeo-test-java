package adeo.leroymerlin.cdp;

import javax.persistence.*;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
public class Event {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    private String title;

    private String imgUrl;

    @OneToMany(fetch=FetchType.EAGER,cascade = {CascadeType.ALL})
    private Set<Band> bands;

    private Integer nbStars;

    private String comment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Set<Band> getBands() {
        return bands;
    }

    public void setBands(Set<Band> bands) {
        this.bands = bands;
    }

    public Integer getNbStars() {
        return nbStars;
    }

    public void setNbStars(Integer nbStars) {
        this.nbStars = nbStars;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Event(Event event){
        this.bands = event.bands;
        this.comment = event.comment;
        this.id = event.id;
        this.imgUrl = event.imgUrl;
        this.nbStars = event.nbStars;
        this.title = event.title;
    }

    public Event(){}

    public Event filterBands(String filter){
        Set<Band> filteredBands = this.bands.stream()
        .map(band -> band.filterMembers(filter))
        .map(band -> band.enrichBand())
        .filter(band -> !band.getMembers().isEmpty())
        .collect(Collectors.toSet());
        
        Event copyEvent = new Event(this);
        copyEvent.setBands(filteredBands);
        return copyEvent;
        
    }

    public Event enrichEvent(){
        Event copyEvent = new Event(this);
        copyEvent.setTitle(copyEvent.title + " [" + Integer.toString(this.bands.size()) + ']');
        return copyEvent;
    }
}
