package adeo.leroymerlin.cdp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Member(Member member){
        this.id = member.id;
        this.name = member.name;
    }

    public Member(){}
    
    public Member filterInternal(String filter){
        boolean filterMatches = this.name.toLowerCase().contains(filter.toLowerCase());
        Member copyMember = new Member(this);
        if (!filterMatches){
            copyMember.setName("");
        }
        return copyMember;
    }

}
