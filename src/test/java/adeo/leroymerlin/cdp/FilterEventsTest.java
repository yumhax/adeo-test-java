package adeo.leroymerlin.cdp;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Collections;
import java.util.List;

import org.junit.runner.RunWith;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FilterEventsTest {

	@Autowired
	private EventRepository er;
	
	@Autowired
	private EventService ev = new EventService(er);

	private String QUERY = "Jasmine";
	private String EXPECTED_QUERY_RESULT = "Queen Jasmine Collier";
	private Integer EXPECTED_QUERY_RESULT_COUNT = 1;
	private String EMPTY_RESULT_QUERY = "fzpeoreijbgkn";
	private String SHORT_QUERY = "a";
	private Integer EXPECTED_SHORT_QUERY_RESULT_COUNT = 5;
	private Integer TOTAL_EVENT_COUNT = 5;
	
	private String EXPECTED_QUERY_EVENT_TITLE = "GrasPop Metal Meeting [1]";
	private String EXPECTED_QUERY_BAND_NAME = "Guns n roses [1]";

	@Test
	public void filterAllShouldReturnEmptyList() throws Exception {
		assertEquals(ev.getFilteredEvents(EMPTY_RESULT_QUERY), Collections.emptyList());
	}

	@Test
	public void filterShouldReturnExpectedResult() throws Exception {
		assertEquals(
			ev.getFilteredEvents(QUERY)
			.get(0)
			.getBands()
			.iterator().next()
			.getMembers()
			.iterator().next()
			.getName().
			toString(),
			 EXPECTED_QUERY_RESULT
			 );
			}
			
	@Test
	public void filterWithQueryShouldReturnExpectedResultCount(){
		List<Event> filteredEvents = ev.getFilteredEvents(QUERY);
		assertEquals(filteredEvents.size(), EXPECTED_QUERY_RESULT_COUNT);
	}

	@Test
	public void filterShouldBeCaseUnsensitive() throws Exception {
		assertEquals(
			ev.getFilteredEvents(QUERY.toLowerCase())
			.get(0)
			.getBands()
			.iterator().next()
			.getMembers()
			.iterator().next()
			.getName().
			toString(),
			 EXPECTED_QUERY_RESULT
		);
		assertEquals(
			ev.getFilteredEvents(QUERY.toUpperCase())
			.get(0)
			.getBands()
			.iterator().next()
			.getMembers()
			.iterator().next()
			.getName().
			toString(),
			 EXPECTED_QUERY_RESULT
		);
	}

	@Test
	public void filterWithShortRequestShouldReturnExpectedResultCount(){
		assertEquals(ev.getFilteredEvents(SHORT_QUERY).size(), EXPECTED_SHORT_QUERY_RESULT_COUNT);
	}

	@Test
	public void filterWithQueenShouldReturnAllResults(){
		List<Event> filteredEvents = ev.getFilteredEvents("queen");
		assertEquals(filteredEvents.size(), TOTAL_EVENT_COUNT);
	}
	
	@Test
	public void enrichEventShouldCountBands(){
		List<Event> filteredEvents = ev.getFilteredEvents(QUERY);
		assertEquals(filteredEvents.get(0).getTitle(), EXPECTED_QUERY_EVENT_TITLE);
	}

	@Test
	public void enrichBandShouldCountMembers(){
		List<Event> filteredEvents = ev.getFilteredEvents(QUERY);
		
		assertEquals(
			filteredEvents
			.get(0)
			.getBands()
			.iterator().next()
			.getName(), 
			EXPECTED_QUERY_BAND_NAME
		);
	}
	

	
}
